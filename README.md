# Haskell Quantitative Finance Library

[![Build Status](https://travis-ci.org/cokleisli/hqfl.svg?branch=master)](https://travis-ci.org/cokleisli/hqfl)
[![Coverage Status](https://coveralls.io/repos/github/cokleisli/hqfl/badge.svg?branch=master)](https://coveralls.io/github/cokleisli/hqfl?branch=master)
[![License](https://img.shields.io/hexpm/l/plug.svg)](https://github.com/cokleisli/hqfl/blob/master/LICENSE)

### Summary

| Instrument | Pricing Methodology | Haskell Implementation |
|------------|---------------------|------------------------|
| Option: Equity | Black Scholes | [BlackScholes](https://github.com/cokleisli/hqfl/blob/master/src/Finance/Hqfl/Pricer/BlackScholes.hs) |
| Option: StockIndex | Black Scholes | [BlackScholes](https://github.com/cokleisli/hqfl/blob/master/src/Finance/Hqfl/Pricer/BlackScholes.hs) |
| Option: Futures | Black 1976 | [Black](https://github.com/cokleisli/hqfl/blob/master/src/Finance/Hqfl/Pricer/Black.hs) |
| Option: Futures | Asay 1982 | [Asay](https://github.com/cokleisli/hqfl/blob/master/src/Finance/Hqfl/Pricer/Asay.hs) |
| Option: Currency | Garman & Kohlhagen 1983 Black-Scholes | [BlackScholes](https://github.com/cokleisli/hqfl/blob/master/src/Finance/Hqfl/Pricer/BlackScholes.hs) |
