-----------------------------------------------------------------------------
-- |
-- Module      :  Finance.Hqfl
-- Copyright   :  (C) 2016 Mika'il Khan
-- License     :  (see the file LICENSE)
-- Maintainer  :  Mika'il Khan <co.kleisli@gmail.com>
-- Stability   :  stable
-- Portability :  portable
--
----------------------------------------------------------------------------   

module Finance.Hqfl
  ( module Finance.Hqfl.Instrument
  ) where

import Finance.Hqfl.Instrument
